﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZadanieRekrutacyjneINNEGRY.helpers
{
    public class WarehouseSortComparer : IComparer<Warehouse>
    {
        public int Compare(Warehouse firstWarehouse, Warehouse secondWarehouse)
        {
            if (firstWarehouse.TotalCountOfProducts > secondWarehouse.TotalCountOfProducts)
                return -1;
            else if (firstWarehouse.TotalCountOfProducts < secondWarehouse.TotalCountOfProducts)
                return 1;
            else
                return secondWarehouse.Name.CompareTo(firstWarehouse.Name);
        }
    }
}