﻿using System.Collections.Generic;
using ZadanieRekrutacyjneINNEGRY.models;

namespace ZadanieRekrutacyjneINNEGRY.helpers
{
    public class ProductSortComparer : IComparer<Product>
    {
        public int Compare(Product firstProduct, Product secondProduct)
        {
            return firstProduct.Code.CompareTo(secondProduct.Code);
        }
    }
}