﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using ZadanieRekrutacyjneINNEGRY.models;

namespace ZadanieRekrutacyjneINNEGRY
{
    public class WarehouseProductCreator
    {
        private readonly StreamReader _warehouseStructureStream;
        private List<Warehouse> Warehouses = new List<Warehouse>();
        public WarehouseProductCreator(StreamReader warehouseStructureStream)
        {
            _warehouseStructureStream = warehouseStructureStream;
        }
        public List<Warehouse> CreateWarehouseStructure()
        {
            string textLine;
            while ((textLine = _warehouseStructureStream.ReadLine()) != null)
            {
                if (textLine.StartsWith("#"))
                    continue;

                string[] lineArrayWithWarehouseData = textLine.Split(";");
                if (lineArrayWithWarehouseData.Length != 3)
                    throw new ArgumentException($"{textLine} has no complete data");

                var product = new Product()
                {
                    Name = lineArrayWithWarehouseData[0],
                    Code = lineArrayWithWarehouseData[1]
                };
                Dictionary<string, int> warehousesWithCount = GetWarehouseNameWithCount(lineArrayWithWarehouseData[2]);
                foreach (var warehouseWithCount in warehousesWithCount)
                {
                    Warehouse warehouse = GetAndCreateIfNotExistWarehouse(warehouseWithCount.Key);
                    warehouse.ProductsWithCount.Add((product, warehouseWithCount.Value));
                    warehouse.TotalCountOfProducts += warehouseWithCount.Value;
                }
            }
            return Warehouses;
        }
        private Dictionary<string, int> GetWarehouseNameWithCount(string value)
        {
            var warenHouseProducts = new Dictionary<string, int>();
            foreach (string warenhouseWithCount in value.Split("|"))
            {
                string[] warenhouseCount = warenhouseWithCount.Split(",");
                warenHouseProducts.Add(warenhouseCount[0], int.Parse(warenhouseCount[1]));
            }
            return warenHouseProducts;
        }

        private Warehouse GetAndCreateIfNotExistWarehouse(string warehouseName)
        {
            Warehouse warehouse = Warehouses.Find(x => x.Name == warehouseName);
            if (warehouse == null)
            {
                warehouse = new Warehouse
                {
                    Name = warehouseName
                };
                Warehouses.Add(warehouse);
            }
            return warehouse;
        }
    }
}