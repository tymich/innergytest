﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ZadanieRekrutacyjneINNEGRY
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("provide local path to file with warehouses structure");
            //provide local txt file path with structure of warehouses
            string pathToFileWithWarehouses = Console.ReadLine();
            try
            {
                using (var fileReader = new StreamReader(pathToFileWithWarehouses))
                {
                    var structureCreator = new WarehouseProductCreator(fileReader);
                    List<Warehouse> wareHouses = structureCreator.CreateWarehouseStructure();
                    Console.WriteLine(new WarehouseStringOutputCreator(wareHouses).GetStringToOutput());
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("path is not correct");
            }
            catch (ArgumentException)
            {
                Console.WriteLine("provided input has no correct format");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
