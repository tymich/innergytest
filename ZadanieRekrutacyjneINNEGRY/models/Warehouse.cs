﻿using System;
using System.Collections.Generic;
using System.Text;
using ZadanieRekrutacyjneINNEGRY.models;

namespace ZadanieRekrutacyjneINNEGRY
{
    public class Warehouse
    {
        public Warehouse()
        {
            this.ProductsWithCount = new List<(Product product, int count)>();
        }

        public string Name { get; set; }
        public List<(Product product, int count)> ProductsWithCount { get; set; }
        public int TotalCountOfProducts { get; set; }
    }
}