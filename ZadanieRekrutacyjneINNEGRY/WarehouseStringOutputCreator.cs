﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZadanieRekrutacyjneINNEGRY.helpers;

namespace ZadanieRekrutacyjneINNEGRY
{
    public class WarehouseStringOutputCreator
    {
        private readonly List<Warehouse> _warehouses;
        private readonly StringBuilder _stringOutput = new StringBuilder();

        public WarehouseStringOutputCreator(List<Warehouse> warehouses)
        {
            _warehouses = warehouses;
        }

        public string GetStringToOutput()
        {
            foreach (Warehouse warehouse in _warehouses.OrderBy(x => x, new WarehouseSortComparer()))
            {
                AddFirstLineString(warehouse);
                AddProductsLinesString(warehouse);
                _stringOutput.AppendLine();
            }
            return _stringOutput.ToString();
        }

        private void AddFirstLineString(Warehouse warehouse)
        {
            _stringOutput.AppendLine($"{warehouse.Name} (total {warehouse.TotalCountOfProducts})");
        }

        private void AddProductsLinesString(Warehouse warehouse)
        {
            foreach (var (product, count) in warehouse.ProductsWithCount.OrderBy(x => x.product, new ProductSortComparer()))
            {
                _stringOutput.AppendLine($"{product.Code}: {count}");
            }
        }
    }
}
